//
//  EventFeedCell.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 5/10/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import UIKit
import Parse

class EventFeedCell: FeedCell {
    
    var events: [PFObject]?
    
    var profileVC : ProfileVC?
    
    
    
//    func getEvents() {
//        print("inside get events")
//        let query = PFQuery(className:"Event")
//        query.whereKey("FoodTruck", equalTo: PFUser.current()?.objectId!)
//        //query.whereKey("End", greaterThan: Date())
//        query.order(byAscending: "Start")
//        query.findObjectsInBackground { (objects, error) in
//            if error == nil {
//                // The find succeeded.
//                print("I successfully retrived the events")
//                print(objects)
//
//                if let objects = objects {
//                    self.events = objects
//                    DispatchQueue.main.async {
//                        self.collectionView.reloadData()
//                    }
//
//                } else {
//                    // Log details of the failure
//                    print("Error: \(error!) \(error!._userInfo ?? "" as AnyObject)")
//                }
//
//            }
//        }
//
//
//    }
    func getEvents() {
        let query = PFQuery(className:"Event")
        query.whereKey("FoodTruck", equalTo: PFUser.current())
        query.order(byAscending: "Start")
        query.whereKey("End", greaterThan: Date())
        
        query.findObjectsInBackground { (objects, error) in
                        if error == nil {
                            // The find succeeded.
                            print("I successfully retrived the events \(objects?.count)")
            
            
                            if let objects = objects {
                                
                                self.events = objects
                                DispatchQueue.main.async {
                                    self.collectionView.reloadData()
                                }
            
                            } else {
                                // Log details of the failure
                                print("Error: \(error!) \(error!._userInfo ?? "" as AnyObject)")
                            }
            
                        }
    }
    }
    
    

    
    override func setupViews() {
        
        getEvents()
    
        setupCV()
        setupLayout()
    
    }
    

  

  
    override func setupCV() {
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .vertical
            //flowLayout.minimumLineSpacing = 0
        }
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(EventCell.self, forCellWithReuseIdentifier: cellID)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(collectionView)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var numItems = 0
        if let count = self.events?.count {
            numItems = count
        }
        return numItems
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! EventCell
        
        cell.event = self.events?[indexPath.row]
        cell.setupCell()
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: 75)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        profileVC?.gotoEditEventVC(event: events![indexPath.item])
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        
        if let cell = collectionView.cellForItem(at: indexPath) as? EventCell {
                
            cell.contentView.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? EventCell {
            
            cell.contentView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }
    
    
    
    func setupLayout(){
        let views = ["cv" : collectionView
            
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-10-[cv]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        

        
        let cvHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[cv]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += cvHorizontal

        addConstraints(allConstraints)
    }
}



class EventCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        
    }
    
    var event : PFObject?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let locationLabel : UILabel = {
        let lb = UILabel()
        lb.font = UIFont.boldSystemFont(ofSize: 12)
        lb.textColor = UIColor.black
        //lb.backgroundColor = UIColor.gray
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let dateLabel : UILabel = {
        let lb = UILabel()
        lb.text = "10/12/18 - 10:00 - 12:00"
        lb.font = UIFont.boldSystemFont(ofSize: 16)
        lb.textColor = UIColor.black
        //lb.backgroundColor = UIColor.gray
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    func setupViews(){
        
        backgroundColor = UIColor.white
        addSubview(dateLabel)
        addSubview(locationLabel)
        setupLayout()
        
    }
    
    func setupCell(){
        if self.event != nil {
            self.locationLabel.text = (event!["Address"] as! String)
            let start = event!["Start"] as! Date
            let end = event!["End"] as! Date
            let date = start.toString(dateFormat: "MM/dd/yy")
            self.dateLabel.text = "\(date)   -   \(start.toString(dateFormat: "h:mm a")) - \(end.toString(dateFormat: "h:mm a"))"
            
        }
        
      
    }
    
    func setupLayout(){
        let views = ["locationlb" : locationLabel,
                     "datelb" : dateLabel
            
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-5-[datelb]-0-[locationlb]-10-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        let locationlbHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[datelb]-5-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += locationlbHorizontal
        
        let datelbHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-15-[locationlb]-5-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += datelbHorizontal
        
        
        
        addConstraints(allConstraints)
    }
}




