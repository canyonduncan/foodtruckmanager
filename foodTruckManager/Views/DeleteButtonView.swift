//
//  DeleteButtonView.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 5/14/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import UIKit
import Parse

class DeleteButtonView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(deleteButton)
        setupViews()
        setupButton()
        
        
    }
    
    func setupButton(){
        //deleteButton.addTarget(self, action: #selector(deletePFObject), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var object:PFObject?
    //var addEventVC:addEventVC?
    
    let deleteButton: UIButton = {
        let bt = UIButton()
        bt.translatesAutoresizingMaskIntoConstraints = false
        bt.backgroundColor = UIColor.red
        bt.setTitle("Delete", for: .normal)
        return bt
        
    }()
    
    @objc func deletePFObject(){
        //object?.deleteInBackground()
        //addEventVC?.navigationController?.popViewController(animated: true)
    }
    
    
    
    func setupViews() {
        let views = ["deleteButton": deleteButton
            
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-15-[deleteButton(25)]",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        
        let cvHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-50-[deleteButton]-50-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += cvHorizontal
        
        
        
        addConstraints(allConstraints)
        
        
    }
}
