//
//  ProfileFeedCell.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 5/11/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import UIKit
import Parse

class ProfileFeedCell: UICollectionViewCell {
    
    
    //var phone = PFUser.current()!["phone"]
    ///var email = PFUser.current()!["email"]
    //var google = PFUser.current()!["googleCalendarID"]
    //var facebook = PFUser.current()!["FacebookURL"]
    //var image : UIImage?
    
    
    
    let phoneLabel : UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.backgroundColor = UIColor.white
        return lb
    }()
    let emailLabel : UILabel = {
        let lb = UILabel()
        lb.backgroundColor = UIColor.white
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    let googleLabel : UILabel = {
        let lb = UILabel()
        lb.backgroundColor = UIColor.white
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    let facebookLabel : UILabel = {
        let lb = UILabel()
        lb.backgroundColor = UIColor.white

        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let imageView : UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = UIColor.purple
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        getImage()
        setupLayout()
        getUser()
    }
    
    func setupLayout(){
        
        
        addSubview(phoneLabel)
        addSubview(imageView)
        addSubview(emailLabel)
        addSubview(facebookLabel)
        addSubview(googleLabel)
        
        
        
        let views = ["phone" : phoneLabel,
                     "email" : emailLabel,
                     "facebook" : facebookLabel,
                     "google" : googleLabel,
                     "image" : imageView
            
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-10-[image(200)]-5-[phone(40)]-0-[email(40)]-0-[facebook(40)]-0-[google(40)]",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        
        
        let imageHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[image]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += imageHorizontal
        
        let phoneHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[phone]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += phoneHorizontal
        
        let emailHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[email]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += emailHorizontal
        
        let facebookHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[facebook]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += facebookHorizontal
        
        let googleHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[google]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += googleHorizontal
        
        addConstraints(allConstraints)
    }
    
    func getUser(){
        
        var user = PFUser.current() as! PFObject
        if let emailaddress = user["email"] {
            self.emailLabel.text = emailaddress as! String
        }
        
        if let fb = user["FacebookURL"] {
            self.facebookLabel.text = fb as! String
        }
        if let phoneNumber = user["phone"]{
            self.phoneLabel.text = phoneNumber as! String
        }
        if let googleCalID = user["googleCalendarID"]{
            self.googleLabel.text = googleCalID as! String
        }
    }
    
    func getImage() {
        DispatchQueue.global(qos: .userInteractive).async {
            // Async background process
            
            if let imageFile : PFFile = PFUser.current()?["Image"] as! PFFile {
                imageFile.getDataInBackground(block: { (data, error) in
                    if error == nil {
                        DispatchQueue.main.async {
                            // Async main thread
                            
                            let image = UIImage(data: data!)
                            self.imageView.image = image
                        }
                    } else {
                        print(error!.localizedDescription)
                    }
                })
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
}
