//
//  SettingsCell.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 5/14/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import UIKit

class SettingsCell: UICollectionViewCell {
    
    
    let nameLabel : UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.text = "Settings"
        return lb
    }()
    
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? UIColor.darkGray : UIColor.white
            nameLabel.textColor = isHighlighted ? UIColor.white : UIColor.black
        }
    }
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        addSubview(nameLabel)
        setupViews()
    }
    
    func setupViews(){
        let views = ["nameLabel" : nameLabel
        
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[nameLabel]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        let nameLabelHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-15-[nameLabel]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += nameLabelHorizontal
        
       
        
        addConstraints(allConstraints)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
