//
//  MenuFeedCell.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 5/10/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import UIKit
import Parse

class MenuFeedCell: FeedCell {
    
   // var menuItems: [PFObject]?
    var profileVC: ProfileVC?
    var menuItems: [PFObject]?
    
    
    override func setupViews() {
        getMenuItems()
        setupCV()
        setupLayout()
    }
    
    
    
    override func setupCV(){
        
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .vertical
            //flowLayout.minimumLineSpacing = 0
        }
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(MenuCell.self, forCellWithReuseIdentifier: cellID)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(collectionView)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        if self.menuItems != nil{
            count = (self.menuItems?.count)!
        }
        return count
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! MenuCell
        let menuItem = self.menuItems?[indexPath.item]
        cell.menuItem = menuItem
        cell.setupCell()
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: 75)
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        
        if let cell = collectionView.cellForItem(at: indexPath) as? MenuCell {
            
            cell.contentView.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? MenuCell {
            
            cell.contentView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        profileVC?.gotoEditMenuItemVC(menuItem: (self.menuItems![indexPath.item]))
    }
    
    func setupLayout(){
        let views = ["cv" : collectionView

            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-10-[cv]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
       
        
        let cvHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[cv]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += cvHorizontal
       
        
        addConstraints(allConstraints)
    }
    
    func getMenuItems(){
        print("inisde get menu items")
        let query = PFQuery(className:"MenuItem")
        query.whereKey("UserID", equalTo: PFUser.current()?.objectId!)
        
        
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let objects = objects {
                    self.menuItems = objects
                    
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
     
                }
            } else {
                // Log details of the failure
                print("Error: \(error!)")
            }
        }
        
        
        
        
        
    }
    
    
    
    
    
}

class MenuCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    var menuItem : PFObject?
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let titleLabel : UILabel = {
        let lb = UILabel()
        lb.font = UIFont.systemFont(ofSize: 24)
        lb.text = "Corndog"
        lb.textColor = UIColor.black
        //lb.backgroundColor = UIColor.gray
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let priceLabel : UILabel = {
        let lb = UILabel()
        lb.font = UIFont.systemFont(ofSize: 24)
        lb.text = "$10"
        lb.textColor = UIColor.black
        //lb.backgroundColor = UIColor.gray
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let descriptionLabel : UILabel = {
        let lb = UILabel()
        lb.font = UIFont.systemFont(ofSize: 12)
        lb.text = "Dogs diped in our famus batter deep fried to perfection."
        lb.textColor = UIColor.black
        //lb.backgroundColor = UIColor.gray
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
   
    
    func setupViews(){
        
        backgroundColor = UIColor.white
        addSubview(priceLabel)
        addSubview(descriptionLabel)
        addSubview(titleLabel)
        setupLayout()
        
    }
    
    func setupCell(){
        if self.menuItem != nil {
            self.titleLabel.text = menuItem!["ItemName"] as? String
            let price : Int = menuItem!["ItemPrice"] as! Int
            let priceString = "$" + String(price)
            self.priceLabel.text = priceString
            self.descriptionLabel.text = menuItem!["ItemDescription"] as? String
        }
    }
    
    func setupLayout(){
        let views = ["title" : titleLabel,
                     "description" : descriptionLabel,
                     "price" : priceLabel
            
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-10-[title(==description)]-10-[description]-5-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        let vertical2Constraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-10-[price(==description)]-10-[description]-5-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += vertical2Constraints
        
        let descriptionlbHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-20-[description]-10-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += descriptionlbHorizontal

     
        let titlelbHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[title]-10-[price(50)]-10-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += titlelbHorizontal
        
        
        
        addConstraints(allConstraints)
    }
}
