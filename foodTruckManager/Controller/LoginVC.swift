//
//  ViewController.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 12/28/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit
import Parse

class LoginVC: UIViewController, UITextFieldDelegate {
    
    
    
    var titleLabel = UILabel(frame: CGRect.zero)
    var username = UITextField(frame: CGRect.zero)
    var password = UITextField(frame: CGRect.zero)
    var login = UIButton(frame: CGRect.zero)
    var createNew = UIButton(frame: CGRect.zero)
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.username.delegate = self
        self.password.delegate = self
        
        titleLabel.textAlignment = .center
        titleLabel.text = "Vendor Login"
        titleLabel.textColor = UIColor.white
        titleLabel.font = titleLabel.font.withSize(30)
        //titleLabel.backgroundColor = UIColor.gray
        titleLabel.textColor = UIColor.black
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.alpha = 0
        self.view.addSubview(titleLabel)
        
        
        login.setTitle("Login", for: UIControlState.normal)
        login.setTitleColor(UIColor.white, for: UIControlState.normal)
        login.backgroundColor = UIColor.green
        login.translatesAutoresizingMaskIntoConstraints = false
        login.addTarget(self, action: #selector(loginButtonPressed), for: .touchUpInside)
        login.layer.cornerRadius = 10
        login.alpha = 0
        login.isEnabled = false
        self.view.addSubview(login)
        
        username.borderStyle = UITextBorderStyle.roundedRect
        username.translatesAutoresizingMaskIntoConstraints = false
        username.returnKeyType = .done
        username.placeholder = "Email"
        username.alpha = 0
        username.textAlignment = .center
        self.view.addSubview(username)
        
        password.borderStyle = UITextBorderStyle.roundedRect
        password.translatesAutoresizingMaskIntoConstraints = false
        password.returnKeyType = .done
        password.placeholder = "Password"
        password.isSecureTextEntry = true
        password.alpha = 0
        password.textAlignment = .center
        self.view.addSubview(password)
        
        createNew.setTitle("Create New Account", for: UIControlState.normal)
        createNew.backgroundColor = UIColor.gray
        createNew.translatesAutoresizingMaskIntoConstraints = false
        createNew.alpha = 0
        createNew.layer.cornerRadius = 10
        createNew.addTarget(self, action: #selector(createNewPressed), for: .touchUpInside)
        self.view.addSubview(createNew)
        
        
        let views = ["titleLabel": titleLabel,
                     "login": login,
                     "username": username,
                     "password": password,
                     "createNew": createNew
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-70-[titleLabel(150)]-50-[username(40)]-20-[password(40)]-20-[login(50)]",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        let vertCreateNew = NSLayoutConstraint.constraints(
            withVisualFormat: "V:[createNew(50)]-20-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += vertCreateNew
        
        let titleHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-30-[titleLabel]-30-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += titleHorizontal
        
        let usernameHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-50-[username]-50-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += usernameHorizontal
        
        let passwordHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-50-[password]-50-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += passwordHorizontal
        
        let loginHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-30-[login]-30-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += loginHorizontal
        
        let createNewHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-30-[createNew]-30-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += createNewHorizontal
        
        self.view.addConstraints(allConstraints)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.8, animations: {
            self.titleLabel.alpha = 1
        })
        UIView.animate(withDuration: 1.0, animations: {
            self.password.alpha = 1
            self.username.alpha = 1
        })
        UIView.animate(withDuration: 1.3, animations: {
            self.login.alpha = 0.3
            self.createNew.alpha = 1
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if (PFUser.current() != nil) {
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let startNavController = storyboard.instantiateViewController(withIdentifier: "startNav") as! UINavigationController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = startNavController
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.checkField(sender: self)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.checkField(sender: self)
    }
    
    func checkField(sender: AnyObject) {
        if (self.username.text?.isEmpty)! || (self.password.text?.isEmpty)!
        {
            login.isEnabled = false
            self.login.alpha = 0.3
        }
        else
        {
            self.login.isEnabled = true
            self.login.alpha = 1
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "createNewAccount") {
            if segue.destination is CreateNewAccountVC{
                
            }
        }
        
    }
    
    @objc func createNewPressed() {
        print("i made it here")
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateNewAccountVC") as! CreateNewAccountVC
        self.present(loginVC, animated: true, completion: nil)
        
    }
    
    @objc func loginButtonPressed() {
        PFUser.logInWithUsername(inBackground: self.username.text!, password:self.password.text!) {
            (user: PFUser?, error: Error?) -> Void in
            if let error = error {
                if let errorString = (error as NSError).userInfo["error"] as? String {
                    NSLog(errorString);
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
                        NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                // Hooray! Let them use the app now.
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let startNavController = storyboard.instantiateViewController(withIdentifier: "startNav") as! UINavigationController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = startNavController
            }
        }
        
        
        
    }
    
}

