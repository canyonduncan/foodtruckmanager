//
//  editProfileVC.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 12/29/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit
import Eureka
import ImageRow
import Parse

class editProfileVC: FormViewController {
    
    var email = ""
    var phone = ""
    var facebook = ""
    var googleID = ""
    var imageData: PFFile?
    var image = UIImage()
    var profileVC:ProfileVC?
    
    var barButtonTitle = "Update"
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let updateButton : UIBarButtonItem = UIBarButtonItem(title: barButtonTitle, style: UIBarButtonItemStyle.done, target: self, action: #selector(editProfileVC.updateUser))
        
        
        self.navigationItem.rightBarButtonItem = updateButton
        
        var user = PFUser.current() as! PFObject
        //self.email = user["email"] as! String
        //self.phone = user["phone"] as! String
        
        if let emailaddress = user["email"] {
            self.email = emailaddress as! String
        }
    
        if let fb = user["FacebookURL"] {
            self.facebook = fb as! String
        }
        if let phoneNumber = user["phone"]{
            self.phone = phoneNumber as! String
        }
        if let googleCalID = user["googleCalendarID"]{
            self.googleID = googleCalID as! String
        }
        
        
       form  +++ Section("Profile Image")
            <<< ImageRow() {
                $0.title = "Image"
                $0.tag = "image"
                $0.value = self.image
                $0.sourceTypes = .PhotoLibrary
                $0.clearAction = .no
                }
                .cellUpdate { cell, row in
                    cell.accessoryView?.layer.cornerRadius = 17
                    cell.accessoryView?.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            }
        
            
            +++ Section("Contact Information")
            <<< EmailRow(){
                $0.tag = "email"
                $0.title = "Email"
                $0.value = self.email
                
                
            }
            <<< PhoneRow(){
                $0.tag = "phone"
                $0.title = "Phone"
                $0.value = self.phone
                
                
            }
            <<< URLRow(){
                $0.tag = "facebook"
                $0.title = "Facebook URL"
                $0.value = URL(string: self.facebook)
            }
        
            +++ Section("Google Calendar")
            <<< TextRow(){
                $0.tag = "googleCalendarID"
                $0.title = "Google Calendar ID"
                $0.value = self.googleID
            }
            
        
       
                
        
        
        // Do any additional setup after loading the view.
    }

    @objc func updateUser() {
        
        let formValues = form.values()
       
        if let emailAddress = formValues["email"]{
            if(emailAddress != nil){
                if(emailAddress as! String != self.email){
                    PFUser.current()!.setObject(self.email, forKey: "email")
                }
            }
        }
    
        if let phoneNumber = formValues["phone"]{
            if(phoneNumber != nil){
                if(phoneNumber as! String != self.phone){
                    PFUser.current()!.setObject(phoneNumber!, forKey: "phone")
                }
                
            }
        }
        
        if let facebookURL = formValues["facebook"] {
            print("it is a value")
            
            if(facebookURL != nil){
                let url = facebookURL as! URL
                let path = url.absoluteString
                if(path != self.facebook){
                    PFUser.current()!.setObject(path, forKey: "FacebookURL")
                }
                
            }
            
        }
        
        if let googleCalID = formValues["googleCalendarID"] {
            if(googleCalID != nil){
                if(googleCalID as! String != self.googleID){
                    PFUser.current()!.setObject(googleCalID!, forKey: "googleCalendarID")
                }
                
            }
        }
        
        if let userImage = formValues["image"] {
            if(userImage != nil){
                if(userImage as! UIImage != self.image){
                    let image = userImage as! UIImage
                    let data = UIImagePNGRepresentation(image)
                    self.imageData = PFFile(name: "img", data: data!)
                    PFUser.current()!.setObject(self.imageData!, forKey: "Image")
                }
                
            }
        }
        
        
        
        
        
        
        
        
        PFUser.current()!.saveInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                // The object has been saved.
                self.navigationController?.popViewController(animated: true)
                self.profileVC?.menu.insideProfileFeedCell?.getUser()
            } else {
                // There was a problem, check error.description
            }
        }
       
        
        
      
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}




