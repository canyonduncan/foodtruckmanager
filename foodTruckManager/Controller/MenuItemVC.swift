//
//  MenuItemVC.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 1/3/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import UIKit
import Eureka
import Parse
import ViewRow

class MenuItemVC: FormViewController {
    
    var menuItem:PFObject?
    var isEditMenuItem = false
    var profileVC:ProfileVC?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
        //print(menuItem)
        //print(isEditMenuItem)
        if(menuItem == nil){
            
        
            form +++ Section("Menu Item")
                <<< TextRow(){ row in
                    row.tag = "itemName"
                    row.title = "Name"
                    row.placeholder = ""
                }
                <<< TextRow(){ row in
                    row.tag = "itemDescription"
                    row.title = "Description"
                    row.placeholder = ""
                }
            
                <<< IntRow(){ row in
                    row.tag = "itemPrice"
                    row.title = "Price"
                    row.placeholder = ""
            }
            
        }
        else{
            form +++ Section("Menu Item")
                <<< TextRow(){ row in
                    row.tag = "itemName"
                    row.title = "Name"
                    row.value = menuItem!["ItemName"] as? String
                }
                <<< TextRow(){ row in
                    row.tag = "itemDescription"
                    row.title = "Description"
                    row.value = menuItem!["ItemDescription"] as? String
                }
                
                <<< IntRow(){ row in
                    row.tag = "itemPrice"
                    row.title = "Price"
                    row.value = menuItem!["ItemPrice"] as? Int
                }
                +++ Section("")
                <<< ViewRow<DeleteMenuItemView>("view") { (row) in
                    //row.title = "Delete" // optional
                    }
                    .cellSetup { (cell, row) in
                        //  Construct the view for the cell
                        cell.view = DeleteMenuItemView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                        cell.view?.object = self.menuItem
                        cell.view?.menuItemVC = self
                        cell.view?.backgroundColor = UIColor.white
            }
        }
        
            
        
        
        
        // Do any additional setup after loading the view.
    }
    
    func saveMenuItem(){
        print("")
        print("")
        print("inside save menu item")
        let menuItem = PFObject(className:"MenuItem")
        let formValues = form.values()
        
        menuItem["ItemName"] = formValues["itemName"]!
        menuItem["ItemDescription"] = formValues["itemDescription"]!
        menuItem["ItemPrice"] = formValues["itemPrice"]!
        //menuItem["FoodTruck"] = PFObject(withoutDataWithClassName:"_User", objectId: PFUser.current()?.objectId)
        menuItem["UserID"] = PFUser.current()?.objectId
        //var relation = PFUser.current()?.relation(forKey: "MenuItem")
        //relation?.add(menuItem)
        
        
        
        menuItem.saveInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                // The object has been saved.
                _ = self.navigationController?.popViewController(animated: true)
                self.profileVC?.menu.insideMenuFeedCell?.getMenuItems()
                print("calling reload items in save menu item")

            } else {
                // There was a problem, check error.description
            }
        }
    }
    
    func updateMenuItem(){
        let formValues = form.values()
        print("")
        print("")
        print("inisde update menu item")
        
        var query = PFQuery(className:"MenuItem")
        query.getObjectInBackground(withId: (menuItem?.objectId)!) {
            (object, error) -> Void in
            if error != nil {
                print(error)
            } else {
                if let object = object {
                    object["ItemName"] = formValues["itemName"]!
                    object["ItemDescription"] = formValues["itemDescription"]!
                    object["ItemPrice"] = formValues["itemPrice"]!
                }
                object!.saveInBackground {
                    (success: Bool, error: Error?) in
                    if (success) {
                        // The object has been saved.
                        _ = self.navigationController?.popViewController(animated: true)
                        
                        self.profileVC?.menu.insideMenuFeedCell?.getMenuItems()
                        print("calling reload items in update men item")
                        
                        
                    } else {
                        // There was a problem, check error.description
                    }
                }
               // self.profileVC?.getMenuItems()
               // _ = self.navigationController?.popViewController(animated: true)
                
            }
        }
    }
    
    private func initialize(){
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveButtonPressed))
        navigationItem.rightBarButtonItem = saveButton
    }
    
    @objc func saveButtonPressed(_ sender: UIBarButtonItem) {
        //_ = navigationController?.popViewController(animated: true)
        var emptyFields = false
        let formValues = form.values()
        if let name = formValues["itemName"]{
            if(name == nil){
                emptyFields = true
                showAlert(title: "Name Field empty", message: "Please give your menu item a name.")
            }
        }
        if let description = formValues["itemDescription"]{
            if(description == nil){
                emptyFields = true
                showAlert(title: "Description Field empty", message: "Please give your menu item a description.")
            }
        }
        if let price = formValues["itemPrice"]{
            if(price == nil){
                emptyFields = true
                showAlert(title: "Price Field empty", message: "Please give your menu item a price.")
            
            }
        }
        
        
            
        if(menuItem != nil && !emptyFields && isEditMenuItem){
            self.isEditMenuItem = false
            updateMenuItem()
        }
        else if (!emptyFields){
              saveMenuItem()
        }
        
        
      
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAlert(title : String, message: String) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        let ok = UIAlertAction(
            title: "OK",
            style: UIAlertActionStyle.default,
            handler: nil
        )
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }

   

}


class DeleteMenuItemView: DeleteButtonView {
    
    var menuItemVC:MenuItemVC?
    
    
    override func setupButton() {
        self.deleteButton.addTarget(self, action: #selector(deleteMenuItemAction), for: .touchUpInside)
        
    }
    
    @objc func deleteMenuItemAction(){
        self.object?.deleteInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                // The object has been saved.
                self.menuItemVC?.navigationController?.popViewController(animated: true)
                self.menuItemVC?.profileVC?.menu.insideMenuFeedCell?.getMenuItems()
                
                
            } else {
                // There was a problem, check error.description
            }
        }
        
        
    }
}
