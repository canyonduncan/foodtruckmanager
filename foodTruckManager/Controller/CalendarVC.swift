//
//  CalendarVC.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 12/28/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit
import FSCalendar
import Parse

class CalendarVC: UIViewController, FSCalendarDataSource, FSCalendarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBAction func importGoogle(_ sender: Any) {
        performSegue(withIdentifier: "showGoogleImport", sender: self)
    }
    
    
    
    @IBAction func addEvent(_ sender: Any) {
        performSegue(withIdentifier: "addEvent", sender: self)
    }
    
    var startTime = Date()
    var endTime = Date()
    var address = ""
    var eventID = ""
    var date = Date()
    var pastLocations = [String]()
    var dateLabel = UILabel(frame: CGRect.zero)
    var calendar = FSCalendar(frame: CGRect.zero)
    var tableView = UITableView(frame: CGRect.zero)
    var importButton = UIButton(frame: CGRect.zero)
    
    var events = [PFObject]()
    var selectedDateEvents = [PFObject]()
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.events.removeAll()
        self.getEvents()
        self.startTime = Date()
        self.endTime = Date()
        self.address = ""
        self.eventID = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.getEvents()
        self.getPassedLocations()
        
        self.title = "Events"
        
        let today = date
        let todayString = today.toString(dateFormat: "MMM dd")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        
        dateLabel.textAlignment = .center
        dateLabel.text = todayString
        dateLabel.textColor = UIColor.black
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(dateLabel)

//        importButton.backgroundColor = UIColor.blue
//        importButton.translatesAutoresizingMaskIntoConstraints = false
//        importButton.addTarget(self, action: #selector(importButtonPressed), for: .touchUpInside)
//        self.view.addSubview(importButton)
        
        self.calendar.dataSource = self
        self.calendar.delegate = self
        self.calendar.setScope(.week, animated: false)
        self.calendar.translatesAutoresizingMaskIntoConstraints = false
        //calendar.allowsMultipleSelection = true
       // calendar.setScope(FSCalendarScope.week, animated: true)
//        calendar.swipeToChooseGesture.isEnabled = true // Swipe-To-Choose
//
//        let scopeGesture = UIPanGestureRecognizer(target: calendar, action: #selector(calendar.handleScopeGesture(_:)));
//        calendar.addGestureRecognizer(scopeGesture)
        
        
        view.addSubview(self.calendar)

        let views = ["calendar": calendar,
                     "dateLabel": dateLabel,
                     "tableView" : tableView
                     //"importButton" : importButton
            ] as [String : Any]

        var allConstraints = [NSLayoutConstraint]()

        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-66-[calendar(200)]-10-[dateLabel]-0-[tableView]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        

        let calendarHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[calendar]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += calendarHorizontal
        
        let dateHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-30-[dateLabel]-30-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += dateHorizontal
        
        let tableViewHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[tableView]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += tableViewHorizontal
        
        


        self.view.addConstraints(allConstraints)
        
        // Do any additional setup after loading the view.
        
    }
    
    @objc func importButtonPressed() {
        
        //let googleView = self.storyboard?.instantiateViewController(withIdentifier: "GoogleViewController") as! GoogleViewController
        //self.prepare(for: <#T##UIStoryboardSegue!#>, sender: <#T##Any?#>)
        //self.present(googleView, animated: true, completion: nil)
        performSegue(withIdentifier: "showGoogleImport", sender: self)
        
    }
    
    /// calendar events
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
        let dateString = self.dateFormatter2.string(from: date)
        
        if self.datesWithEvent.contains(dateString) {
            return 1
        }
//        if self.datesWithMultipleEvents.contains(dateString) {
//            return 3
//        }
        
        return 0
    }
    
    var datesWithEvent = [String]()
    //var datesWithEvent = ["2018-01-03", "2018-01-02", "2018-01-05", "2018-01-04"]
    //var datesWithMultipleEvents = ["2018-01-10", "2018-01-07"]
    
    
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    /// end calendar events
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date) {
        self.selectedDateEvents = [PFObject]()
        
        for event in self.events {
            let startTime = event["Start"] as! Date
            let startTimeString = startTime.toString(dateFormat: "yyyyMMdd")
            let dateString = date.toString(dateFormat: "yyyyMMdd")
            print(startTimeString)
            print(startTime)
            print("---")
            print(dateString)
            print(date)
            if(startTimeString == dateString){
                
                self.selectedDateEvents.append(event)
            }
            
        }
        self.tableView.reloadData()
        self.date = date
        self.dateLabel.text = date.toString(dateFormat: "MMM dd")
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "addEvent") {
            let viewController:addEventVC = segue.destination as! addEventVC
            viewController.startTime = self.startTime
            viewController.endTime = self.endTime
            //viewController.pastLocations = self.pastLocations
            viewController.address = self.address
            if(self.address != ""){
                viewController.barButttonTitle = "Update"
            }
            if(self.eventID != ""){
                viewController.eventID = self.eventID
            }
        }
        if(segue.identifier == "showGoogleImport"){
            let viewController:GoogleViewController = segue.destination as! GoogleViewController
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.selectedDateEvents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let start = self.selectedDateEvents[indexPath.row]["Start"] as! Date
        let startTime = start.toString(dateFormat: "hh:mm")
        cell.textLabel?.text = startTime + " - " + (self.selectedDateEvents[indexPath.row]["Address"] as! String)
        
        //cell.detailTextLabel?.text = startTime
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.address = self.selectedDateEvents[indexPath.row]["Address"] as! String
        self.startTime = self.selectedDateEvents[indexPath.row]["Start"] as! Date
        self.endTime = self.selectedDateEvents[indexPath.row]["End"] as! Date
        self.eventID = self.selectedDateEvents[indexPath.row].objectId!
        performSegue(withIdentifier: "addEvent", sender: self)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            
                selectedDateEvents[indexPath.row].deleteInBackground()
                selectedDateEvents.remove(at: indexPath.row)
                self.tableView.reloadData()
                self.calendar.reloadData()
        }
    }
    
    
    
    
    
    func getEvents() {
        let query = PFQuery(className:"Event")
        query.whereKey("UserID", equalTo: PFUser.current()?.objectId!)
        query.whereKey("End", greaterThan: Date())
        query.order(byAscending: "Start")
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                // The find succeeded.
                if let objects = objects {
                    for object in objects {
                        let startTime = object["Start"] as! Date
                        let startDate = startTime.toString(dateFormat: "yyyy-MM-dd")
                        self.datesWithEvent.append(startDate)
                        self.events.append(object)
                        
                    }
                    self.calendar.reloadData()
                    self.tableView.reloadData()
                    
                } else {
                    // Log details of the failure
                    print("Error: \(error!) \(error!._userInfo ?? "" as AnyObject)")
                }
                
            }
        }
        
        
    }
    
    func getPassedLocations() {
        
        let query = PFQuery(className:"Event")
        query.whereKey("UserID", equalTo: PFUser.current()?.objectId!)
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                // The find succeeded.
                if let objects = objects {
                    for object in objects {
                        
                        if(self.pastLocations.contains(object["Address"] as! String) == false){
                            self.pastLocations.append(object["Address"] as! String)
                        }
                        
                    }
                    
                    
                } else {
                    // Log details of the failure
                    print("Error: \(error!) \(error!._userInfo ?? "" as AnyObject)")
                }
                
            }
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
