//
//  ProfileVC.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 12/28/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit
import Parse
import GoogleAPIClientForREST
import GoogleSignIn

class ProfileVC: UIViewController {
   
    
   
    var imageView = UIImageView(frame: CGRect.zero)
    
    
    var menuItemToEdit:PFObject?
    var editMenuItem = false
    
    var pastLocations = [String]()
    lazy var menu: Menu = {
        let m = Menu()
        m.profileVC = self
        return m
    }()
    
    
    
    
    lazy var settingsLauncher : SettingsLauncher = {
        let sl = SettingsLauncher()
        sl.profileVC = self
        return sl
    }()
    
    func showControllerForSettings(name: String){
        switch name {
        case "Add Event":
            let vc = addEventVC()
            vc.profileVC = self
            navigationController?.pushViewController(vc, animated: true)
        case "Import Event":
            let importEvent = GoogleViewController()
            importEvent.profileVC = self
            navigationController?.pushViewController(importEvent, animated: true)
        case "Edit Profile":
            let vc = editProfileVC()
            vc.image = self.imageView.image!
            vc.profileVC = self
            navigationController?.pushViewController(vc, animated: true)
        case "Add Menu Item":
            let vc = MenuItemVC()
            vc.profileVC = self
            navigationController?.pushViewController(vc, animated: true)
        case "Log Out":
            PFUser.logOut()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let startNavController = storyboard.instantiateInitialViewController()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = startNavController
            
            
        default:
            print("default")
        }
        
        
    
        
    }
    
    @IBAction func editProfle(_ sender: Any) {
        //performSegue(withIdentifier: "editProfile", sender: self)
        settingsLauncher.showSettings()
    }
    
    func gotoEditEventVC(event: PFObject){
        let vc = addEventVC()
        vc.event = event
        vc.profileVC = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoEditMenuItemVC(menuItem: PFObject){
        let vc = MenuItemVC()
        vc.menuItem = menuItem
        vc.isEditMenuItem = true
        vc.profileVC = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //getMenuItems()
        //let indexPath = IndexPath(row: 2, section: 0)
        //self.menu.collectionView.reloadItems(at: [indexPath])
        //getMenuItems()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //getMenuItems()
        
        //PFUser.logOut()
        
        
        self.title = PFUser.current()?["FoodTruckCompany"] as! String
        
        
        DispatchQueue.global(qos: .userInteractive).async {
            // Async background process
            
            if let imageFile : PFFile = PFUser.current()?["Image"] as! PFFile {
                imageFile.getDataInBackground(block: { (data, error) in
                    if error == nil {
                        DispatchQueue.main.async {
                            // Async main thread
                            
                            let image = UIImage(data: data!)
                            self.imageView.image = image
                        }
                    } else {
                        print(error!.localizedDescription)
                    }
                })
            }
        }
    
        

        //self.collectionView.backgroundColor = UIColor.white
        self.menu.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(menu)
        
        //self.menuBar.translatesAutoresizingMaskIntoConstraints = false
        //self.view.addSubview(self.menuBar)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.black
        //self.view.addSubview(imageView)
        setupConstraints()
        
        self.getPassesLocations()
        
    
    }
    
    func getPassesLocations(){
        
        let query = PFQuery(className:"Event")
        query.whereKey("FoodTruck", equalTo: PFUser.current() as Any)
        query.limit = 15
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                // The find succeeded.
                print("I successfully retrived the events")
                
                
                if let objects = objects {
                    for object in objects {
                        let address = object["Address"] as! String
                        if(!(self.pastLocations.contains(address))){
                            self.pastLocations.append(address)
                        }
                    }
                    //self.events = objects
                    //self.tableView.reloadData()
                    print(self.pastLocations)
                    self.reloadInputViews()
                    
                    
                } else {
                    // Log details of the failure
                    print("Error: \(error!) \(error!._userInfo ?? "" as AnyObject)")
                }
                
            }
        }
    }
    
    
    func setupConstraints(){
        let views = ["imageView": imageView,
                     "menu" : menu
                    
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        

        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[menu]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        
        let cvHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[menu]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += cvHorizontal
        
     
        
        self.view.addConstraints(allConstraints)
    }

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    

    
    override func prepare(for segue: UIStoryboardSegue!, sender: Any?) {
        if (segue.identifier == "editProfile") {
            let viewController:editProfileVC = segue!.destination as! editProfileVC
            viewController.image = self.imageView.image!
            
            
        }
        
        if (segue.identifier == "addEventVC") {
            let viewController:addEventVC = segue!.destination as! addEventVC
        }
        
        
        if(segue.identifier == "addMenuItem" && self.editMenuItem){
            if let viewController:MenuItemVC = segue!.destination as! MenuItemVC{
                viewController.isEditMenuItem = true
                viewController.menuItem = self.menuItemToEdit
            }
            
        }
        if(segue.identifier == "addMenuItem"){
            let viewController:MenuItemVC = segue!.destination as! MenuItemVC
            
        }
        
    }
  
    
    
    
    
    

  

}




