//
//  CreateNewAccountVC.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 12/28/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit
import Parse

class CreateNewAccountVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    
    
    var imagePicker = UIImagePickerController()
    var imageData: PFFile?
    
    var email = UITextField(frame: CGRect.zero)
    var password = UITextField(frame: CGRect.zero)
    var companyName = UITextField(frame: CGRect.zero)
    var phoneNumber = UITextField(frame: CGRect.zero)
    var menu = UITextView(frame: CGRect.zero)
    var uploadImage = UIButton(frame: CGRect.zero)
    var createAccount = UIButton(frame: CGRect.zero)
    var imageView = UIImageView(frame: CGRect.zero)
    var menuLabel = UILabel(frame: CGRect.zero)
    var imageLabel = UILabel(frame: CGRect.zero)
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        // Do any additional setup after loading the view.
        self.email.delegate = self
        self.password.delegate = self
        self.phoneNumber.delegate = self
        self.companyName.delegate = self
        self.title = "Create Account"
        
        
        email.translatesAutoresizingMaskIntoConstraints = false
        email.layer.cornerRadius = 5
        email.placeholder = "Email"
        //email.backgroundColor = UIColor.blue
        email.textAlignment = .center
        email.alpha = 0
        
        self.view.addSubview(email)
        
        password.translatesAutoresizingMaskIntoConstraints = false
        password.layer.cornerRadius = 5
        password.placeholder = "Password"
        password.isSecureTextEntry = true
        //password.backgroundColor = UIColor.green
        password.textAlignment = .center
        password.alpha = 0
        self.view.addSubview(password)
        
        companyName.translatesAutoresizingMaskIntoConstraints = false
        companyName.placeholder = "Company Name"
        companyName.textAlignment = .center
        companyName.alpha = 0
        self.view.addSubview(companyName)
        
        phoneNumber.translatesAutoresizingMaskIntoConstraints = false
        phoneNumber.placeholder = "Phone Number"
        phoneNumber.textAlignment = .center
        phoneNumber.alpha = 0
        self.view.addSubview(phoneNumber)
        
        menuLabel.translatesAutoresizingMaskIntoConstraints = false
        menuLabel.text = "Menu"
        menuLabel.textAlignment = .center
        menuLabel.alpha = 0
        //menuLabel.backgroundColor = UIColor.gray
        //self.view.addSubview(menuLabel)
        
        menu.translatesAutoresizingMaskIntoConstraints = false
        menu.layer.cornerRadius = 5
        menu.layer.borderColor = UIColor.gray.cgColor
        menu.layer.borderWidth = 1
        menu.alpha = 0
        self.view.addSubview(menu)
        
        imageLabel.translatesAutoresizingMaskIntoConstraints = false
        imageLabel.text = "Photo"
        //imageLabel.backgroundColor = UIColor.blue
        imageLabel.textAlignment = .center
        imageLabel.alpha = 0
        self.view.addSubview(imageLabel)
        
        uploadImage.translatesAutoresizingMaskIntoConstraints = false
        uploadImage.backgroundColor = UIColor.darkGray
        uploadImage.setTitle("Upload", for: UIControlState.normal)
        uploadImage.titleLabel?.textColor = UIColor.black
        uploadImage.layer.cornerRadius = 5
        uploadImage.layer.borderWidth = 1
        uploadImage.layer.borderColor = UIColor.gray.cgColor
        uploadImage.addTarget(self, action: #selector(uploadImageTapped), for: .touchUpInside)
        uploadImage.alpha = 0
        self.view.addSubview(uploadImage)
        
        imageView.backgroundColor = UIColor.gray
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 4
        imageView.clipsToBounds = true
        imageView.alpha = 0
        self.view.addSubview(imageView)
        
        createAccount.translatesAutoresizingMaskIntoConstraints = false
        createAccount.setTitle("Create Account", for: UIControlState.normal)
        createAccount.backgroundColor = UIColor.green
        createAccount.layer.cornerRadius = 5
        createAccount.alpha = 0
        createAccount.addTarget(self, action: #selector(createAccountTapped), for: .touchUpInside)
        self.view.addSubview(createAccount)
        
        
        let views = [   "email": email,
                        "password": password,
                        "companyName": companyName,
                        "phoneNumber": phoneNumber,
                        "menu": menu,
                        "uploadImage": uploadImage,
                        "createAccount": createAccount,
                        "imageView": imageView,
                        "menuLabel": menuLabel,
                        "imageLabel": imageLabel
            ] as [String : Any]
        
        
        var allConstraints = [NSLayoutConstraint]()
        
        let colBottomVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:[createAccount(50)]-15-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += colBottomVerticalConstraints
        
        let colOneVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-80-[email(40)]-15-[phoneNumber(40)]-20-[imageLabel(50)]-5-[imageView]-50-[createAccount]",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += colOneVerticalConstraints
        
        let colTwoVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-80-[password(40)]-15-[companyName(40)]-30-[uploadImage(20)]",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += colTwoVerticalConstraints
        
//        let colThreeVerticalConstraints = NSLayoutConstraint.constraints(
//            withVisualFormat: "V:|-30-[uploadImage(20)]",
//            options: [],
//            metrics: nil,
//            views: views)
//        allConstraints += colThreeVerticalConstraints
        
        
        
        let rowOneHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-15-[email(==password)]-15-[password]-15-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += rowOneHorizontal
        
        let rowTwoHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-15-[phoneNumber(==companyName)]-15-[companyName]-15-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += rowTwoHorizontal
        
//        let rowThreeHorizontal = NSLayoutConstraint.constraints(
//            withVisualFormat: "H:|-125-[menuLabel]-125-|",
//            options: [],
//            metrics: nil,
//            views: views)
//        allConstraints += rowThreeHorizontal
//
//        let rowFourHorizontal = NSLayoutConstraint.constraints(
//            withVisualFormat: "H:|-15-[menu]-15-|",
//            options: [],
//            metrics: nil,
//            views: views)
//        allConstraints += rowFourHorizontal
        
        let rowFiveHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-125-[imageLabel]-0-[uploadImage(75)]-50-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += rowFiveHorizontal
        
        let rowSixHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-15-[imageView]-15-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += rowSixHorizontal
        
        let rowSevenHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-75-[createAccount]-75-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += rowSevenHorizontal
        
        self.view.addConstraints(allConstraints)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setBottomBorderToTextFields()
        UIView.animate(withDuration: 0.2, animations: {
            self.email.alpha = 1
            
        })
        UIView.animate(withDuration: 0.4, animations: {
            self.password.alpha = 1
            
        })
        UIView.animate(withDuration: 0.6, animations: {
            self.phoneNumber.alpha = 1
            
        })
        UIView.animate(withDuration: 0.8, animations: {
            self.companyName.alpha = 1
            
        })
        UIView.animate(withDuration: 1.0, animations: {
            self.menuLabel.alpha = 1
            self.menu.alpha = 1
            
        })
        UIView.animate(withDuration: 1.2, animations: {
            self.imageLabel.alpha = 1
            self.imageView.alpha = 1
            self.uploadImage.alpha = 1
        })
        UIView.animate(withDuration: 1.4, animations: {
            self.createAccount.alpha = 1
        })
        
        //self.setTopBorderToTextFields()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setBottomBorderToTextFields()  {
        
        let bottomLineEmail = CALayer()
        bottomLineEmail.frame = CGRect(x: 0, y: self.email.frame.height - 1, width: self.email.frame.width, height: 1)
        bottomLineEmail.backgroundColor = UIColor.gray.cgColor // background color
        self.email.borderStyle = UITextBorderStyle.none // border style
        self.email.layer.addSublayer(bottomLineEmail)
        
        let bottomLinePassword = CALayer()
        bottomLinePassword.frame = CGRect(x: 0, y: self.password.frame.height - 1, width: self.password.frame.width, height: 1)
        bottomLinePassword.backgroundColor = UIColor.gray.cgColor
        self.password.borderStyle = UITextBorderStyle.none
        self.password.layer.addSublayer(bottomLinePassword)
        
        let bottomLinePhone = CALayer()
        bottomLinePhone.frame = CGRect(x: 0, y: self.phoneNumber.frame.height - 1, width: self.phoneNumber.frame.width, height: 1)
        bottomLinePhone.backgroundColor = UIColor.gray.cgColor
        self.phoneNumber.borderStyle = UITextBorderStyle.none
        self.phoneNumber.layer.addSublayer(bottomLinePhone)
        
        let bottomLineCompany = CALayer()
        bottomLineCompany.frame = CGRect(x: 0, y: self.companyName.frame.height - 1, width: self.companyName.frame.width, height: 1)
        bottomLineCompany.backgroundColor = UIColor.gray.cgColor
        self.companyName.borderStyle = UITextBorderStyle.none
        self.companyName.layer.addSublayer(bottomLineCompany)
        
        
    }
    
    func setTopBorderToTextFields()  {
        
        let topLine = CALayer()
        topLine.frame = CGRect(x: 0, y: self.email.frame.height - 40, width: self.email.frame.width, height: 1)
        topLine.backgroundColor = UIColor.gray.cgColor // background color
        email.borderStyle = UITextBorderStyle.none // border style
        email.layer.addSublayer(topLine)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            self.imageView.contentMode = .scaleAspectFit
            self.imageView.image = pickedImage
            let image = pickedImage
            let data = UIImagePNGRepresentation(image)
            self.imageData = PFFile(name: "img", data: data!)
            
        }
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion:nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "cancelCreateAccount") {
            segue.destination is LoginVC
        }
        if(segue.identifier == "createAccount"){
            segue.destination is ProfileVC
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func createAccountTapped() {
        if ((self.email.text?.isEmpty)! || (self.password.text?.isEmpty)! || (self.companyName.text?.isEmpty)! || (self.phoneNumber.text?.isEmpty)! ||  (self.imageData == nil)){
            
            let alertController = UIAlertController(title: "Empty Fields", message: "Account Not Created. Please fill out all the fields.", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            
        }
        else{
            let user = PFUser()
            user.username = email.text
            user.password = password.text
            user.email = email.text
            user["phone"] = phoneNumber.text
            user["FoodTruckCompany"] = companyName.text
            user["Image"] = self.imageData
            
            user.signUpInBackground(block: { (succeed, error) -> Void in
                
                if let error = error {
                    print(error.localizedDescription)
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
                        //NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else {
                    //NSLog("Signed up!");
                    self.performSegue(withIdentifier: "createAccount", sender: self)
                    
                }
            })
        }
    }
    
    @objc func uploadImageTapped() {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
}

