//
//  addEventVC.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 12/28/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit
import Eureka
import CoreLocation
import Parse
import ViewRow

class addEventVC: FormViewController {
    
    var profileVC:ProfileVC?
    var event: PFObject?
    var startTime = Date()
    var endTime = Date()
    var address = ""
    var geoLocation = CLLocationCoordinate2D()
    var location = String()
    
    var barButttonTitle = "Add"
    var eventID = ""
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get current user
        if(event != nil){
            self.startTime = event!["Start"] as! Date
            self.endTime = event!["End"] as! Date
            self.address = event!["Address"] as! String
            self.barButttonTitle = "Update"
            self.eventID = (event?.objectId)!
        }
        
        let addButton : UIBarButtonItem = UIBarButtonItem(title: barButttonTitle, style: UIBarButtonItemStyle.done, target: self, action: #selector(addEventVC.addEventPressed))
        
        
        self.navigationItem.rightBarButtonItem = addButton
        
        if(self.event != nil) {
            editEvent()
        }
        else {
            addEvent()
        }
        
       
        
        
    }
    
    func addEvent() {
        form +++ Section("Date")
            <<< DateTimeInlineRow(){
                $0.tag = "startTime"
                $0.title = "Start Time"
                $0.value = self.startTime
                
            }
            <<< DateTimeInlineRow(){
                $0.tag = "endTime"
                $0.title = "End Time"
                $0.value = self.endTime
                
            }
            
            +++ Section("Location")
            <<< SegmentedRow<String>("segments"){
                $0.options = ["New Location", "Past Location"]
                $0.value = "New Location"
                
            }
            
            +++ Section(){
                $0.tag = "newLocation"
                $0.hidden = "$segments != 'New Location'"
            }
            <<< TextRow(){ row in
                row.tag = "location"
                row.title = "Address"
                row.value = self.address
            }
            
            +++ Section() {
                $0.tag = "passedLocation"
                $0.hidden = "$segments != 'Past Location'"
            }
            <<< PushRow<String> {
                $0.title = "Past Locations"
                $0.options = self.profileVC?.pastLocations
                $0.tag = "pastLocation"
                
        }
    }
    
    func editEvent () {
        form +++ Section("Date")
            <<< DateTimeInlineRow(){
                $0.tag = "startTime"
                $0.title = "Start Time"
                $0.value = self.startTime
                
            }
            <<< DateTimeInlineRow(){
                $0.tag = "endTime"
                $0.title = "End Time"
                $0.value = self.endTime
                
            }
            
            +++ Section("Location")
            <<< SegmentedRow<String>("segments"){
                $0.options = ["New Location", "Past Location"]
                $0.value = "New Location"
                
            }
            
            +++ Section(){
                $0.tag = "newLocation"
                $0.hidden = "$segments != 'New Location'"
            }
            <<< TextRow(){ row in
                row.tag = "location"
                row.title = "Address"
                row.value = self.address
            }
            
            +++ Section() {
                $0.tag = "passedLocation"
                $0.hidden = "$segments != 'Past Location'"
            }
            <<< PushRow<String> {
                $0.title = "Past Locations"
                $0.options = self.profileVC?.pastLocations
                $0.tag = "pastLocation"
                
            }
            
            +++ Section("")
            <<< ViewRow<DeleteEventView>("view") { (row) in
                //row.title = "Delete" // optional
                }
                .cellSetup { (cell, row) in
                    //  Construct the view for the cell
                    cell.view = DeleteEventView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                    cell.view?.object = self.event
                    cell.view?.addEventVC = self
                    cell.view?.backgroundColor = UIColor.white
                }
    }
    
    @objc func addEventPressed() {
        
        let formValues = form.values()
        let segments = formValues["segments"] as! String
        //var location = ""
        
        if(segments == "Past Location"){
            if let passedLocation = formValues["pastLocation"] {
                if(passedLocation != nil) {
                    self.location = passedLocation as! String
                }
            }
        }
        if(segments == "New Location"){
            if let newLocation = formValues["location"] {
                if(newLocation != nil) {
                    self.location = newLocation as! String
                }
            }
        }
        
        
        
        let geocoder = CLGeocoder()
    
        geocoder.geocodeAddressString(self.location, completionHandler: {(placemarks, error) -> Void in
            if((error) != nil){
                print("Error", error ?? "")
                let alertController = UIAlertController(title: "Invalid Address", message: "Address could not be found. Please enter a new Address.", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
            if let placemark = placemarks?.first {
                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                self.geoLocation = coordinates
                if(self.barButttonTitle == "Add"){
                        self.saveEvent(coordinates: coordinates)
                }
                else{
                        self.updateEvent(coordinates: coordinates)
                }
                
            }
        })
    
        
    }
    
    func saveEvent(coordinates: CLLocationCoordinate2D) {
        
        let lat:CLLocationDegrees = coordinates.latitude
        let long:CLLocationDegrees = coordinates.longitude
        let eventLocation = PFGeoPoint(latitude: lat, longitude: long)
        
        let formValues = form.values()
        
        let event = Event(start: formValues["startTime"] as! Date, end: formValues["endTime"] as! Date, address: self.location, geopoint: eventLocation, foodtruck: PFUser.current()!)
        
        event.saveInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                // The object has been saved.
                _ = self.navigationController?.popViewController(animated: true)
                self.profileVC?.menu.insideEventFeedCell?.getEvents()
            } else {
                // There was a problem, check error.description
            }
        }
    }
    
    func updateEvent(coordinates: CLLocationCoordinate2D){
        let formValues = form.values()
        let query = PFQuery(className:"Event")
        query.getObjectInBackground(withId: self.eventID) {
            (object, error) -> Void in
            if error != nil {
                print(error as Any)
            } else {
                if let object = object {
                    object["Start"] = formValues["startTime"]!
                    object["End"] = formValues["endTime"]!
                    object["Address"] = self.address
                    object["geoPoint"] = PFGeoPoint(latitude: coordinates.latitude, longitude: coordinates.longitude)
                }
                object!.saveInBackground {
                    (success: Bool, error: Error?) in
                    if (success) {
                        // The object has been saved.
                        _ = self.navigationController?.popViewController(animated: true)
                        self.profileVC?.menu.insideEventFeedCell?.getEvents()
                    } else {
                        // There was a problem, check error.description
                    }
                }
                
            }
        }
        
    }
    
    
    
    
    
    
    
    
}

class DeleteEventView: DeleteButtonView {
    
    var addEventVC:addEventVC?
    
    
    override func setupButton() {
        self.deleteButton.addTarget(self, action: #selector(deleteEventAction), for: .touchUpInside)
    
    }
    
    @objc func deleteEventAction(){
        self.object?.deleteInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                // The object has been saved.
                self.addEventVC?.navigationController?.popViewController(animated: true)
                self.addEventVC?.profileVC?.menu.insideEventFeedCell?.getEvents()
            } else {
                // There was a problem, check error.description
            }
        }
        
        
    }
    
    
}








