//
//  MenuItem.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 7/18/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import Foundation
import Parse

class MenuItem : PFObject {
    @NSManaged var ItemName : String
    @NSManaged var ItemPrice : Double
    @NSManaged var ItemDescription : String
    
    override init() {
        super.init()
    }
    
    init(itemName : String, itemDescription : String, itemPrice : Double) {
        super.init()
        self.ItemName = itemName
        self.ItemDescription = itemDescription
        self.ItemPrice = itemPrice
    }
}
