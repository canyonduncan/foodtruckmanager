//
//  EventDataProvider.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 7/18/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import Foundation
import Parse

class DataProvider {
    var events : [Event] = []
    var menuItems : [MenuItem] = []
    var user : PFUser = PFUser.current()!
    
    init() {
        self.queryEvents()
        self.queryMenuItems()
    }
    
    func queryEvents() {
        
        let query = PFQuery(className:"Event")
        query.whereKey("FoodTruck", equalTo: PFUser.current()!)
        //query.cachePolicy = .cacheElseNetwork
        query.order(byAscending: "Start")
        
        query.findObjectsInBackground { (events, error) in
            if error == nil {
                if let events = events as? [Event] {
                    self.events = events
                } else {
                    // Log details of the failure
                    print("Error: \(error!) \(error!._userInfo ?? "" as AnyObject)")
                }
                
            }
        }
    }
    
    func queryMenuItems() {
        let query = PFQuery(className:"MenuItem")
        query.whereKey("FoodTruck", equalTo: PFUser.current()!)
       // query.cachePolicy = .cacheElseNetwork
        
        query.findObjectsInBackground { (menuItems, error) in
            if error == nil {
                if let menuItems = menuItems as? [MenuItem] {
                    self.menuItems = menuItems
                } else {
                    // Log details of the failure
                    print("Error: \(error!) \(error!._userInfo ?? "" as AnyObject)")
                }
                
            }
        }
    }
    
    func addMenuItem(item : MenuItem) {
        self.menuItems.append(item)
    }
    
    func addEvent(event : Event){
        self.events.append(event)
    }
}
