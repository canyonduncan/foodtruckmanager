//
//  Event.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 7/18/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import Parse
import Foundation

class Event : PFObject {
    
    @NSManaged var Start : Date
    @NSManaged var End : Date
    @NSManaged var Address : String
    @NSManaged var geoPoint : PFGeoPoint
    @NSManaged var FoodTruck : PFObject
    
    override init() {
        super.init()
    }
    
    init(start: Date, end : Date, address : String, geopoint : PFGeoPoint, foodtruck : PFUser) {
        super.init()
        self.Start = start
        self.End = end
        self.Address = address
        self.geoPoint = geopoint
        self.FoodTruck = PFObject(withoutDataWithClassName: "_User", objectId: foodtruck.objectId)
        
    }
    
    func saveEvent() {
        self.saveInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                print("Saved the event")
                // The object has been saved.
                //_ = self.navigationController?.popViewController(animated: true)
                //self.profileVC?.menu.insideEventFeedCell?.getEvents()
            } else {
                // There was a problem, check error.description
            }
        }
    }
}

extension Event : PFSubclassing {
    static func parseClassName() -> String {
        return "Event"
    }
    
    
    
    
    
}
