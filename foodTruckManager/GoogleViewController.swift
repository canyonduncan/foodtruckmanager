//
//  GoogleViewController.swift
//  foodTruckManager
//
//  Created by Canyon Duncan on 5/4/18.
//  Copyright © 2018 Canyon Duncan. All rights reserved.
//

import GoogleAPIClientForREST
import GoogleSignIn
import UIKit
import CoreLocation
import Parse

class GoogleViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    
    // If modifying these scopes, delete your previously saved credentials by
    // resetting the iOS simulator or uninstall the app.
    private let scopes = [kGTLRAuthScopeCalendarReadonly]
    
    private let service = GTLRCalendarService()
    let signInButton = GIDSignInButton()
    
    var tableView = UITableView(frame: CGRect.zero)
    var textField = UITextField(frame: CGRect.zero)
    
    var events = [GTLRCalendar_Event]()
    var pfObjectEvents = [PFObject]()
    var calendarID = ""
    var profileVC : ProfileVC?
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        signInButton.isHidden = false
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        let addButton : UIBarButtonItem = UIBarButtonItem(title: "Add", style: UIBarButtonItemStyle.done, target: self, action: #selector(GoogleViewController.addButtonPressed))
        
        self.navigationItem.rightBarButtonItem = addButton
        
        // Configure Google Sign-in.
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signInSilently()
        
        let user = PFUser.current() as! PFObject
        //self.email = user["email"] as! String
        //self.phone = user["phone"] as! String
        
        if let googleCalID = user["googleCalendarID"] {
            self.calendarID = googleCalID as! String
        }
        
        self.signInButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(signInButton)
        
        self.textField.delegate = self
        self.textField.translatesAutoresizingMaskIntoConstraints = false
        self.textField.isHidden = true
        //self.textField.placeholder = "Google CalendarID"
        
        if(self.calendarID != ""){
                self.textField.text = self.calendarID
        }
        else {
            self.textField.placeholder = "Google CalendarID"
        }
        
        
        self.view.addSubview(self.textField)

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.allowsMultipleSelection = true
        self.view.addSubview(tableView)
        self.tableView.isHidden = true
        
        setupViews()
        
        
    }
    
    
    func setupViews() {
        let views = ["signInButton": signInButton,
                     "tableView": tableView,
                     "textField": textField
            
            ] as [String : Any]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-100-[signInButton(100)]",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        let tableViewverticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-66-[textField]-30-[tableView]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += tableViewverticalConstraints
        
        let buttonHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-100-[signInButton]-100-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += buttonHorizontal
        
        let textFieldHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[textField]-10-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += textFieldHorizontal
        
        let tableViewHorizontal = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[tableView]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += tableViewHorizontal
        
        
        
        self.view.addConstraints(allConstraints)
    }
    
    @objc func addButtonPressed(_ sender: UIBarButtonItem) {
        
        
        if(tableView.indexPathsForSelectedRows?.count == nil){
            showAlert(title: "No Events Select", message: "Please select the events you want to add")
        }
        else{
            addEvents()
        }
        
    }
    
    func convertToPFObject(event: GTLRCalendar_Event, indexPath: IndexPath){
        let temp = PFObject(className: "Event")
        //print(event.location)
        let start = event.start!.dateTime ?? event.start!.date!
        
        let end = event.end!.dateTime ?? event.end!.date!
        temp["Start"] = start.date
        temp["End"] = end.date
        temp["Address"] = event.location
        //temp["geoPoint"] = //getGeoLocation(location: event.location!)
        temp["FoodTruck"] = PFUser.current()
        
        let geocoder = CLGeocoder()
        
        
        geocoder.geocodeAddressString(event.location!, completionHandler: {(placemarks, error) -> Void in
            if((error) != nil){
                print("Error", error ?? "")
               
            }
            if let placemark = placemarks?.first {
                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                temp["geoPoint"] = PFGeoPoint(latitude: coordinates.latitude, longitude: coordinates.longitude)
                self.saveEvent(event: temp, indexPath: indexPath)
            }
        })
        
    }
    
    func saveEvent(event: PFObject, indexPath: IndexPath){
            event.saveInBackground {
                (success: Bool, error: Error?) in
                if (success) {
                    
                    DispatchQueue.main.async { [unowned self] in
                        //self.tableView.cellForRow(at: indexPath)?.detailTextLabel?.text = "Success"
                        //self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                        //self.tableView.reloadData()
                        //self.tableView.reloadData()
                        
                        
                    }
                    //self.tableView.reloadData()
                    print("the event has been saved")
                    self.profileVC?.menu.insideEventFeedCell?.getEvents()
                    // The object has been saved.
                    //_ = self.navigationController?.popViewController(animated: true)
                    //self.tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.green
    
                } else {
                    self.tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.red
                    print("the event has not been saved")
                    // There was a problem, check error.description
                    //self.tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.red
                }
            }
    }
    
    func addEvents(){
        
        var index:[IndexPath] = tableView.indexPathsForSelectedRows!
        for path in index {
            convertToPFObject(event: events[path.row], indexPath: path)
        }
        
    }
    
   
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            showAlert(title: "Authentication Error", message: error.localizedDescription)
            self.service.authorizer = nil
        } else {
            self.signInButton.isHidden = true
            self.tableView.isHidden = false
            self.textField.isHidden = false
            
            self.service.authorizer = user.authentication.fetcherAuthorizer()
            fetchEvents()
        }
    }
    
    // Construct a query and get a list of upcoming events from the user calendar
    func fetchEvents() {
        
        if(self.calendarID == ""){
            calendarID = "primary"
            
        }
        let query = GTLRCalendarQuery_EventsList.query(withCalendarId: calendarID)
        query.maxResults = 10
        query.timeMin = GTLRDateTime(date: Date())
        query.singleEvents = true
        query.orderBy = kGTLRCalendarOrderByStartTime
        service.executeQuery(
            query,
            delegate: self,
            didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:)))
    }
    
    // Display the start dates and event summaries in the UITextView
    @objc func displayResultWithTicket(
        ticket: GTLRServiceTicket,
        finishedWithObject response : GTLRCalendar_Events,
        error : NSError?) {
        
        if let error = error {
            showAlert(title: "Error", message: error.localizedDescription)
            return
        }
        self.events.removeAll()
        
        if let events = response.items, !events.isEmpty {
            for event in events {
                self.events.append(event)
                
                print(event)
                //outputText += "\(startString) - \(event.summary!)\n"
            }
            self.tableView.reloadData()
        } else {
            //outputText = "No upcoming events found."
        }
        //output.text = outputText
    }
    
    
    // Helper for showing an alert
    func showAlert(title : String, message: String) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        let ok = UIAlertAction(
            title: "OK",
            style: UIAlertActionStyle.default,
            handler: nil
        )
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.events.count
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        //cell.backgroundColor = UIColor.clear
        var outputText = ""
        let event = self.events[indexPath.row]
        let start = event.start!.dateTime ?? event.start!.date!
        let startString = DateFormatter.localizedString(
            from: start.date,
            dateStyle: .short,
            timeStyle: .short)
        outputText += "\(startString) - \(event.summary!)\n"
        cell.textLabel?.text = outputText
       
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(tableView.indexPathsForSelectedRows?.count)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.calendarID = textField.text!
        fetchEvents()
        return true;
    }
    
    
}
